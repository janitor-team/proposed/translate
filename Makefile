install: 
	install -d $(DESTDIR)/usr/bin
	cp translate $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/etc
	cp translate.conf $(DESTDIR)/etc
	install -d $(DESTDIR)/usr/share/doc

clean: 
	rm -f *~
